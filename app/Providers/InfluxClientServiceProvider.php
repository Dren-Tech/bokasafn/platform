<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use InfluxDB\Client;
use function foo\func;

class InfluxClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Client::class, function ($app) {
            return new Client(env("INFLUX_HOST"), env("INFLUX_PORT"));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
